C:\Projetos\Templates\Docker_NodeJs_WebApi> 
    docker build -t danielfr/docker_node_template .

    docker run -p 3000:3000 -d danielfr/docker_node_template

    docker ps (look container running)
    docker stop CONTAINER_ID (stop container)
    docker rm CONTAINER_ID (remove container)

    docker-compose up