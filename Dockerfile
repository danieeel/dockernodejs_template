FROM node:alpine

WORKDIR //Projetos//Templates

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "start"]